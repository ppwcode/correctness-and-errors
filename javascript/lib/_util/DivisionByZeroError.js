/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

const assert = require('assert').strict

/**
 * @constructor
 * @param {number} numerator
 */
function DivisionByZeroError (numerator) {
  assert(typeof numerator === 'number')
  assert(!Number.isNaN(numerator))
  assert(Number.isFinite(numerator))

  const err = new Error()
  err.name = 'DivisionByZeroError'
  err.numerator = numerator
  Object.setPrototypeOf(err, Object.getPrototypeOf(this))
  Error.captureStackTrace(err, DivisionByZeroError)
  return err
}

DivisionByZeroError.prototype = Object.create(Error.prototype, {
  constructor: {
    value: Error,
    enumerable: false,
    writable: true,
    configurable: true
  }
})

Object.setPrototypeOf(DivisionByZeroError, Error)

module.exports = DivisionByZeroError
