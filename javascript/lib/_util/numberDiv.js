/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

const DivisionByZeroError = require('./DivisionByZeroError')
const assert = require('assert').strict

/**
 * Introduced because JavaScript does not throw Errors when dividing by `0` (but rather returns
 * `INFINITY`), or when dividing `0` by `0` (but rather returns `NaN`).
 *
 * @param {number} numerator
 * @param {number} denominator
 * @returns {number} `n /d`
 * @throws {DivisionByZeroError}
 */
function numberDiv (numerator, denominator) {
  assert(typeof numerator === 'number')
  assert(!Number.isNaN(numerator))
  assert(Number.isFinite(numerator))
  assert(typeof denominator === 'number')
  assert(!Number.isNaN(denominator))
  assert(Number.isFinite(denominator))

  if (denominator === 0) {
    throw new DivisionByZeroError(numerator)
  }

  return numerator / denominator
}

module.exports = numberDiv
