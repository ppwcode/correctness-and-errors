const assert = require('assert')
const { Tape } = require('./Tape')

class ASTNode {
  invariants () {
    return [
      /**
       * @this ASTNode
       * @return {boolean}
       */
      function () {
        return typeof this.getSymbol() === 'string'
      },
      /**
       * @this ASTNode
       * @return {boolean}
       */
      function () {
        return this.getSymbol() === this.getSymbol().trim()
      }
    ]
  }

  /**
   * @param {string} symbol
   */
  constructor (symbol) {
    assert(typeof symbol === 'string')
    assert(symbol === symbol.trim())

    this._symbol = symbol
  }

  getSymbol () {
    return this._symbol
  }

  /**
   * @param {Tape} tape
   * @return - result === this.tape.lastResult()
   * @return {number}
   */
  /* istanbul ignore next */
  evaluate (tape) {
    assert(tape instanceof Tape)

    assert(false, 'abstract')
  }
}

module.exports = { ASTNode }
