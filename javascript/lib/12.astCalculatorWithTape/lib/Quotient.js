const { BinaryOperation } = require('./BinaryOperation')
const { DivideByZero } = require('./DivideByZero')
const { Tape } = require('./Tape')

const dummyTape = new Tape()

class Quotient extends BinaryOperation {
  invariants () {
    return super.invariants().concat([
      function () {
        return this.getSymbol() === '/'
      }
    ])
  }

  /**
   * @param {ASTNode} a
   * @param {ASTNode} b
   */
  constructor (a, b) {
    super('/', a, b)
  }

  /**
   * @param {Tape} tape
   * @return - result === this.tape.lastResult()
   * @return {number} this.getA().evaluate(tape) / this.getB().evaluate(tape)
   */
  evaluate (tape) {
    if (this._b.evaluate(dummyTape) <= 0) {
      throw new DivideByZero()
    }

    const result = this._a.evaluate(tape) / this._b.evaluate(tape)
    tape.register(result)
    return result
  }
}

module.exports = { Quotient }
