const { CalculatorException } = require('./CalculatorException')

class DivideByZero extends CalculatorException {}

module.exports = { DivideByZero }
