const { isNatural } = require('./is')
const assert = require('assert')
const { Tape } = require('./Tape')
const { ASTNode } = require('./ASTNode')

class NaturalLiteral extends ASTNode {
  invariants () {
    return super.invariants().concat([
      function () {
        return this.getSymbol() === 'ℕ'
      }
    ])
  }

  /**
   * @param {number} natural
   */
  constructor (natural) {
    assert(isNatural(natural))

    super('ℕ')

    /** @type {number} */ this._natural = natural
  }

  /**
   * @param {Tape} tape
   * @return - result === this.tape.lastResult()
   * @return {number}
   */
  evaluate (tape) {
    assert(tape instanceof Tape)

    tape.register(this._natural)
    return this._natural
  }
}

module.exports = { NaturalLiteral }
