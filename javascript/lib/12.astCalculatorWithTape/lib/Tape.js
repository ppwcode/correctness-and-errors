const { isPositiveRational } = require('./is')
const assert = require('assert')

class NoResults extends Error {
  constructor () {
    super()
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

class Tape {
  invariants () {
    return [
      /**
       * @this Tape
       * @return {boolean}
       */
      function () {
        return Array.isArray(this.getResults())
      },
      /**
       * @this Tape
       * @return {boolean}
       */
      function () {
        return this.getResults().every(r => isPositiveRational(r))
      },
      /**
       * @this Tape
       * @return {boolean}
       */
      function () {
        return (this.getResults().length === 0) === this.isEmpty()
      }
    ]
  }

  /**
   * @return - this.isEmpty()
   */
  constructor () {
    /** @type {number[]} */ this._results = []
  }

  /**
   * Basic inspector
   *
   * @return {boolean}
   */
  isEmpty () {
    return this._results.length <= 0
  }

  /**
   * @param {number} result
   * @return {void}
   */
  register (result) {
    assert(isPositiveRational(result))

    this._results.unshift(result)
  }

  /**
   * @return {number}
   *
   * ### Exceptional conditions
   * - NoResults ⇒ { this.isEmpty() }
   */
  getLastResult () {
    if (this._results.length <= 0) {
      throw new NoResults()
    }
    return this._results[0]
  }

  /**
   * Basic inspector
   *
   * @return {number[]}
   */
  getResults () {
    return this._results.slice()
  }

  /**
   * @return - this !== result
   * @return {Tape} result.getResults() deep equals this.getResults
   */
  clone () {
    const result = new Tape()
    result._results = this.getResults()
    return result
  }
}

module.exports = { NoResults, Tape }
