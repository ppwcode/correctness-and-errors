/**
 * @param {any} v
 * @return {boolean}
 */
function isNatural (v) {
  return Number.isInteger(v) && v >= 0
}

/**
 * @param {any} v
 * @return {boolean}
 */
function isPositiveRational (v) {
  return typeof v === 'number' && v >= 0 && v !== Number.POSITIVE_INFINITY
}

module.exports = { isNatural, isPositiveRational }
