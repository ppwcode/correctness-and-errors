const assert = require('assert')
const { ASTNode } = require('./ASTNode')

class BinaryOperation extends ASTNode {
  invariants () {
    return super.invariants().concat([
      /**
       * @this BinaryOperation
       * @return {boolean}
       */
      function () {
        return this.getA() instanceof ASTNode
      },
      /**
       * @this BinaryOperation
       * @return {boolean}
       */
      function () {
        return this.getB() instanceof ASTNode
      }
    ])
  }

  /**
   * @param {string} symbol
   * @param {ASTNode} a
   * @param {ASTNode} b
   */
  constructor (symbol, a, b) {
    assert(typeof symbol === 'string')
    assert(symbol === symbol.trim())
    assert(a instanceof ASTNode)
    assert(a instanceof ASTNode)

    super(symbol)

    /** @type {ASTNode} */ this._a = a
    /** @type {ASTNode} */ this._b = b
  }

  /**
   * Basic inspector
   *
   * @return {ASTNode}
   */
  getA () {
    return this._a
  }

  /**
   * Basic inspector
   *
   * @return {ASTNode}
   */
  getB () {
    return this._b
  }
}

module.exports = { BinaryOperation }
