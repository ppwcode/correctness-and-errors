const { BinaryOperation } = require('./BinaryOperation')
const { Tape } = require('./Tape')
const assert = require('assert')

class Sum extends BinaryOperation {
  invariants () {
    return super.invariants().concat([
      function () {
        return this.getSymbol() === '+'
      }
    ])
  }

  /**
   * @param {ASTNode} a
   * @param {ASTNode} b
   */
  constructor (a, b) {
    super('+', a, b)
  }

  /**
   * @param {Tape} tape
   * @return - result === this.tape.lastResult()
   * @return {number} this.getA().evaluate(tape) + this.getB().evaluate(tape)
   */
  evaluate (tape) {
    assert(tape instanceof Tape)

    const result = this._a.evaluate(tape) + this._b.evaluate(tape)
    tape.register(result)
    return result
  }
}

module.exports = { Sum }
