class CalculatorException extends Error {
  constructor () {
    super()
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor)
  }
}

module.exports = { CalculatorException }
