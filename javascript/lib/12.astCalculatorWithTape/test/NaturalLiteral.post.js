const {
  constructor: astNodeConstructor,
  evaluate: astNodeEvaluate,
  evaluateException: astNodeEvaluateException
} = require('./ASTNode.post')
const { Tape } = require('../lib/Tape')

/**
 * @param {number} natural
 * @param {NaturalLiteral} naturalLiteral
 * @return {void}
 */
function constructor (natural, naturalLiteral) {
  astNodeConstructor(naturalLiteral.getSymbol(), naturalLiteral)

  const tape = new Tape()
  naturalLiteral.evaluate(tape).should.equal(natural)
}

module.exports = { constructor, evaluate: astNodeEvaluate, evaluateException: astNodeEvaluateException }
