const {
  constructor: astNodeConstructor,
  evaluate: astNodeEvaluate,
  evaluateException: astNodeEvaluateException
} = require('./ASTNode.post')

/**
 * @param {string} symbol
 * @param {ASTNode} a
 * @param {ASTNode} b
 * @param {BinaryOperation} binaryOperation
 * @return {void}
 */
function constructor (symbol, a, b, binaryOperation) {
  astNodeConstructor(symbol, binaryOperation)

  binaryOperation.getA().should.equal(a)
  binaryOperation.getB().should.equal(b)
}

module.exports = { constructor, evaluate: astNodeEvaluate, evaluateException: astNodeEvaluateException }
