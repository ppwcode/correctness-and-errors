/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const testName = require('../../../test/_testName')
const { NoResults, Tape } = require('../lib/Tape')
const should = require('should')

describe(testName(module), function () {
  describe('NoResults', function () {
    it('can be constructed', function () {
      const result = new NoResults()
      console.log(result)
    })
  })
  describe('Tape', function () {
    describe('#constructor', function () {
      it('can be constructed', function () {
        const result = new Tape()
        console.log(result)

        // nominal postconditions
        result.isEmpty().should.be.true()
        // invariants
        result.invariants().forEach(invar => {
          invar.call(result).should.be.true()
        })
      })
    })

    describe('instance', function () {
      beforeEach(function () {
        this.instance = new Tape()
      })

      after(function () {
        delete this.instance
      })

      describe('#register', function () {
        it('registers', function () {
          const aPositiveRational = 235.42323

          this.instance.register(aPositiveRational)

          // nominal postconditions
          const lastResult = this.instance.getLastResult()
          lastResult.should.equal(aPositiveRational)
          // inertia
          const results = this.instance.getResults()
          results.length.should.equal(1)
          // invariants
          this.instance.invariants().forEach(invar => {
            invar.call(this.instance).should.be.true()
          })
        })
        it('registers several times', function () {
          const aPositiveRational = 235.42323
          const before = 10
          for (let i = 0; i < before; i++) {
            this.instance.register(i)
          }
          const oldResults = this.instance.getResults().slice()
          console.log(oldResults)

          this.instance.register(aPositiveRational)

          // nominal postconditions
          const lastResult = this.instance.getLastResult()
          lastResult.should.equal(aPositiveRational)
          // inertia, but (un)shifted
          const results = this.instance.getResults()
          console.log(results)
          results.length.should.equal(before + 1)
          oldResults.forEach((or, i) => {
            results[i + 1].should.equal(or)
          })
          // invariants
          this.instance.invariants().forEach(invar => {
            invar.call(this.instance).should.be.true()
          })
        })
      })

      describe('#getLastResult', function () {
        it('works', function () {
          const aPositiveRational = 235.42323
          this.instance.register(aPositiveRational)
          const oldResults = this.instance.getResults()

          const lastResult = this.instance.getLastResult()

          // nominal postconditions
          lastResult.should.equal(oldResults[0])
          // inertia
          const results = this.instance.getResults()
          results.length.should.equal(1)
          // invariants
          this.instance.invariants().forEach(invar => {
            invar.call(this.instance).should.be.true()
          })
        })
        it('works with several registrations', function () {
          const aPositiveRational = 235.42323
          const before = 10
          for (let i = 0; i < before; i++) {
            this.instance.register(i)
          }
          this.instance.register(aPositiveRational)
          const oldResults = this.instance.getResults().slice()
          console.log(oldResults)

          const lastResult = this.instance.getLastResult()

          // nominal postconditions
          lastResult.should.equal(oldResults[0])
          // inertia
          const results = this.instance.getResults()
          oldResults.forEach((or, i) => {
            results[i].should.equal(or)
          })
          // invariants
          this.instance.invariants().forEach(invar => {
            invar.call(this.instance).should.be.true()
          })
        })
        it('throws when empty', function () {
          try {
            this.instance.getLastResult()
            should.fail()
          } catch (err) {
            console.log(err)
            err.should.be.instanceof(NoResults)

            // inertia
            this.instance.isEmpty().should.be.true()
            // invariants
            this.instance.invariants().forEach(invar => {
              invar.call(this.instance).should.be.true()
            })
          }
        })
      })
      describe('#clone', function () {
        it('works', function () {
          for (let i = 0; i < before; i++) {
            this.instance.register(i)
          }

          const clone = this.instance.clone()

          clone.should.not.equal(this.instance)
          clone.getResults().should.deepEqual(this.instance.getResults())
          // invariants
          this.instance.invariants().forEach(invar => {
            invar.call(this.instance).should.be.true()
          })
          // invariants
          clone.invariants().forEach(invar => {
            invar.call(clone).should.be.true()
          })
        })
      })
    })
  })
})
