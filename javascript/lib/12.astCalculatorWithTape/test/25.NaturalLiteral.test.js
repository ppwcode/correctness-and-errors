/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const testName = require('../../../test/_testName')
const { NaturalLiteral } = require('../lib/NaturalLiteral')
const { Tape } = require('../lib/Tape')
const { constructor, evaluate, evaluateException } = require('./NaturalLiteral.post')

describe(testName(module), function () {
  describe('#constructor', function () {
    it('can be constructed', function () {
      const natural = 42
      const result = new NaturalLiteral(natural)

      constructor(natural, result)
    })
    it('can be evaluated', function () {
      const natural = 42
      const tape = new Tape()
      for (let i = 100001; i <= 100010; i++) {
        tape.register(i)
      }
      const oldTape = tape.clone()
      const subject = new NaturalLiteral(natural)

      try {
        const result = subject.evaluate(tape)

        evaluate(oldTape, subject, tape, result)
      } catch (err) {
        evaluateException(oldTape, subject, tape, err)
      }
    })
  })
})
