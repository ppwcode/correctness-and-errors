const {
  constructor: binaryOperationConstructor,
  evaluate: binaryOperationEvaluate,
  evaluateException: binaryOperationEvaluateException
} = require('./BinaryOperation.post')
const { Tape } = require('../lib/Tape')

/**
 * @param {ASTNode} a
 * @param {ASTNode} b
 * @param {Product} product
 * @return {void}
 */
function constructor (a, b, product) {
  binaryOperationConstructor(product.getSymbol(), a, b, product)
}

/**
 * @param {Tape} oldTape
 * @param {Product} product
 * @param {Tape} tape
 * @param {number} result
 * @return {void}
 */
function evaluate (oldTape, product, tape, result) {
  binaryOperationEvaluate(oldTape, product, tape, result)
  const dummyTape = new Tape()
  result.should.equal(product.getA().evaluate(dummyTape) * product.getB().evaluate(dummyTape))
}

module.exports = { constructor, evaluate, evaluateException: binaryOperationEvaluateException }
