/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const testName = require('../../../test/_testName')
const { Tape } = require('../lib/Tape')
const { constructor, evaluate, evaluateException } = require('./Product.post')
const { Product } = require('../lib/Product')
const { as, bs } = require('./_naturals')
const { NaturalLiteral } = require('../lib/NaturalLiteral')

describe(testName(module), function () {
  describe('#constructor', function () {
    it('can be constructed', function () {
      const a = new NaturalLiteral(42)
      const b = new NaturalLiteral(35)
      const result = new Product(a, b)

      constructor(a, b, result)
    })
  })
  describe('#evaluate', function () {
    as.forEach(a => {
      bs.forEach(b => {
        it(`works for (${a}, ${b})`, function () {
          const tape = new Tape()
          for (let i = 100001; i <= 100010; i++) {
            tape.register(i)
          }
          const oldTape = tape.clone()
          const subject = new Product(a, b)

          try {
            const result = subject.evaluate(tape)

            evaluate(oldTape, subject, tape, result)
          } catch (err) {
            evaluateException(oldTape, subject, tape, err)
          }
        })
      })
    })
  })
})
