/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const testName = require('../../../test/_testName')
const { Tape } = require('../lib/Tape')
const { NaturalLiteral } = require('../lib/NaturalLiteral')
const { Sum } = require('../lib/Sum')
const { Product } = require('../lib/Product')
const { Quotient } = require('../lib/Quotient')

describe(testName(module), function () {
  it('(23 + 45) * 123 / (14 + 8)', function () {
    const tree = new Quotient(
      new Product(new Sum(new NaturalLiteral(23), new NaturalLiteral(45)), new NaturalLiteral(123)),
      new Sum(new NaturalLiteral(14), new NaturalLiteral(18))
    )

    const tape = new Tape()

    const result = tree.evaluate(tape)

    console.log(tape.getResults())
    console.log(result)
  })
})
