const { NaturalLiteral } = require('../lib/NaturalLiteral')

const as = [0, 1, 4, 12, Number.MAX_SAFE_INTEGER].map(a => new NaturalLiteral(a))
const bs = [0, 1, 5, 113, Number.MAX_SAFE_INTEGER].map(b => new NaturalLiteral(b))

module.exports = { as, bs }
