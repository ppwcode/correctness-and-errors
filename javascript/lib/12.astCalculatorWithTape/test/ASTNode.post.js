const { isPositiveRational } = require('../lib/is')
const { CalculatorException } = require('../lib/CalculatorException')

/**
 * @param {string} symbol
 * @param {ASTNode} astNode
 */
function constructor (symbol, astNode) {
  console.dir(astNode)
  // nominal postconditions
  astNode.getSymbol().should.equal(symbol)
  // invariants
  astNode.invariants().forEach(invar => {
    invar.call(astNode).should.be.true()
  })
}

/**
 * @param {Tape} oldTape
 * @param {ASTNode} astNode
 * @param {Tape} tape
 * @param {number} result
 * @return {void}
 */
function evaluate (oldTape, astNode, tape, result) {
  console.log('tape:', tape.getResults())
  console.log('result:', result)
  isPositiveRational(result).should.be.true()
  tape.getLastResult().should.equal(result)
  const tapeResults = tape.getResults()
  const oldResults = oldTape.getResults()
  for (let i = oldResults.length - 1; i >= 0; i--) {
    tapeResults[i + (tapeResults.length - oldResults.length)].should.equal(oldResults[i])
  }
}

/**
 * @param {Tape} oldTape
 * @param {ASTNode} astNode
 * @param {Tape} tape
 * @param {any} err
 * @return {void}
 */
function evaluateException (oldTape, astNode, tape, err) {
  console.log('tape:', tape.getResults())
  console.log('err:', err)

  err.should.be.instanceof(CalculatorException)
  const tapeResults = tape.getResults()
  const oldResults = oldTape.getResults()
  tapeResults.length.should.equal(oldResults.length)
  tapeResults.forEach((tr, i) => {
    tr.should.equal(oldResults[i])
  })
}

module.exports = { constructor, evaluate, evaluateException }
