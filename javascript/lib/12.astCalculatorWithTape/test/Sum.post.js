const {
  constructor: binaryOperationConstructor,
  evaluate: binaryOperationEvaluate,
  evaluateException: binaryOperationEvaluateException
} = require('./BinaryOperation.post')
const { Tape } = require('../lib/Tape')

/**
 * @param {ASTNode} a
 * @param {ASTNode} b
 * @param {Sum} sum
 * @return {void}
 */
function constructor (a, b, sum) {
  binaryOperationConstructor(sum.getSymbol(), a, b, sum)
}

/**
 * @param {Tape} oldTape
 * @param {Sum} sum
 * @param {Tape} tape
 * @param {number} result
 * @return {void}
 */
function evaluate (oldTape, sum, tape, result) {
  binaryOperationEvaluate(oldTape, sum, tape, result)
  const dummyTape = new Tape()
  result.should.equal(sum.getA().evaluate(dummyTape) + sum.getB().evaluate(dummyTape))
}

module.exports = { constructor, evaluate, evaluateException: binaryOperationEvaluateException }
