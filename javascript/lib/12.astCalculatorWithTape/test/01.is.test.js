/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const testName = require('../../../test/_testName')
const { isNatural, isPositiveRational } = require('../lib/is')

const nonNumbers = [
  null,
  undefined,
  {},
  [],
  'a string',
  '',
  true,
  false,
  Number.NaN,
  Number.NEGATIVE_INFINITY,
  Number.POSITIVE_INFINITY
]
const negativeNonNaturals = [Number.MIN_SAFE_INTEGER, -1, -Math.PI]
const positiveNonNaturals = [Number.MIN_VALUE, Math.PI]
const nonNaturals = negativeNonNaturals.concat(positiveNonNaturals)
const naturals = [0, 1, 4, 12, Number.MAX_SAFE_INTEGER, Number.MAX_VALUE]
const positiveNumbers = positiveNonNaturals.concat(naturals)

describe(testName(module), function () {
  describe('#isNatural', function () {
    describe('non-numbers', function () {
      nonNumbers.forEach(c => {
        it(`returns false for non-number ${c} (${JSON.stringify(c)})`, function () {
          const result = isNatural(c)
          result.should.be.false()
        })
      })
    })
    describe('non-naturals', function () {
      nonNaturals.forEach(c => {
        it(`returns false for non-natural ${c}`, function () {
          const result = isNatural(c)
          result.should.be.false()
        })
      })
    })
    describe('naturals', function () {
      naturals.forEach(c => {
        it(`returns false for natural ${c}`, function () {
          const result = isNatural(c)
          result.should.be.true()
        })
      })
    })
  })

  describe('#isPositiveRational', function () {
    describe('non-numbers', function () {
      nonNumbers.forEach(c => {
        it(`returns false for non-number ${c} (${JSON.stringify(c)})`, function () {
          const result = isPositiveRational(c)
          result.should.be.false()
        })
      })
    })
    describe('negative numbers', function () {
      negativeNonNaturals.forEach(c => {
        it(`returns false for negative numbers ${c}`, function () {
          const result = isPositiveRational(c)
          result.should.be.false()
        })
      })
    })
    describe('positive numbers', function () {
      positiveNumbers.forEach(c => {
        it(`returns false for positive number ${c}`, function () {
          const result = isPositiveRational(c)
          result.should.be.true()
        })
      })
    })
  })
})
