const {
  constructor: binaryOperationConstructor,
  evaluate: binaryOperationEvaluate,
  evaluateException: binaryOperationEvaluateException
} = require('./BinaryOperation.post')
const { Tape } = require('../lib/Tape')
const { DivideByZero } = require('../lib/DivideByZero')

/**
 * @param {ASTNode} a
 * @param {ASTNode} b
 * @param {Quotient} quotient
 * @return {void}
 */
function constructor (a, b, quotient) {
  binaryOperationConstructor(quotient.getSymbol(), a, b, quotient)
}

/**
 * @param {Tape} oldTape
 * @param {Quotient} quotient
 * @param {Tape} tape
 * @param {number} result
 * @return {void}
 */
function evaluate (oldTape, quotient, tape, result) {
  binaryOperationEvaluate(oldTape, quotient, tape, result)
  const dummyTape = new Tape()
  result.should.equal(quotient.getA().evaluate(dummyTape) / quotient.getB().evaluate(dummyTape))
}

/**
 * @param {Tape} oldTape
 * @param {Quotient} quotient
 * @param {Tape} tape
 * @param {any} err
 * @return {void}
 */
function evaluateException (oldTape, quotient, tape, err) {
  binaryOperationEvaluateException(oldTape, quotient, tape, err)
  err.should.be.instanceof(DivideByZero)
  const dummyTape = new Tape()
  quotient
    .getB()
    .evaluate(dummyTape)
    .should.be.belowOrEqual(0)
}

module.exports = { constructor, evaluate, evaluateException }
