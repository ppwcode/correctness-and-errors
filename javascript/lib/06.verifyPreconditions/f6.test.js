/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

const f = require('./f6')

/* eslint-env mocha */
/* global results:true */

describe('f6', function () {
  beforeEach(function () {
    // noinspection JSUndefinedPropertyAssignment
    /**
     * @type {Array<number>}
     */
    global.results = [5, 8, -5, Math.E, 0] // eslint-disable-line no-global-assign
  })

  afterEach(function () {
    // noinspection JSUnresolvedVariable
    delete global.results
  })

  const as = [0, 1, 4, 12, Number.MAX_SAFE_INTEGER]
  const bs = [0, 1, 5, 113, Number.MAX_SAFE_INTEGER]

  as.forEach(a => {
    bs.forEach(b => {
      it(`works for f(${a}, ${b})`, function () {
        const result = f(a, b)

        result.should.be.a.Number() // NOTE: On a computer, all numbers are rationals
        result.should.be.greaterThanOrEqual(0)
        results[results.length - 1].should.equal(result)
      })
    })
  })
})
