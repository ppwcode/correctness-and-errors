/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

const f = require('./f6')
const { g6a, g6b, g6c } = require('./g6')(f)

/* eslint-env mocha */
/* global results:true */

describe('g6', function () {
  beforeEach(function () {
    // noinspection JSUndefinedPropertyAssignment
    /**
     * @type {Array<number>}
     */
    global.results = [5, 8, -5, Math.E, 0] // eslint-disable-line no-global-assign
  })

  afterEach(function () {
    // noinspection JSUnresolvedVariable
    delete global.results
  })

  it('g6a works, but is wrong', function () {
    const result = g6a()
    console.log(result)
    console.log(results)
  })

  it('g6b works, but is wrong', function () {
    const result = g6b()
    console.log(result)
    console.log(results)
  })

  it('g6c does not work?', function () {
    const result = g6c()
    console.log(result)
    console.log(results)
  })
})
