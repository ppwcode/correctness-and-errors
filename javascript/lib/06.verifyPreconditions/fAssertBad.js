/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* global results:true */

const assert = require('assert')

/**
 * @param {number} a
 * @param {number} b
 * @return {number} a positive rational number (ℚ0+)
 * @return - results[results.length - 1] === result
 */
function fAssert (a, b) {
  if (!Number.isInteger(a) || a < 0) {
    throw new Error('a must be a natural')
  }
  if (!Number.isInteger(b) || b < 0) {
    throw new Error('b must be a natural')
  }
  /* istanbul ignore next: TODO: I don't seem to be able to cover this, although it is */
  if (!Array.isArray(results)) {
    throw new Error('results must be an Array')
  }

  const result = a / b
  assert(typeof result === 'number') // in computers, all numbers are rationals
  assert(result >= 0, 'result must be a _positive_ rational') // NOTE: assert is better documentation!
  results.push(result)
  return result
}

module.exports = fAssert
