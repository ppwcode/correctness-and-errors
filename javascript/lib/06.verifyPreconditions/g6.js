/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

function g6 (f) {
  return {
    g6a: function g6a () {
      // NOTE: works, but not correct
      return f(-8, 4)
    },
    g6b: function g6b () {
      // NOTE: works, but not correct
      return f(8, Math.PI)
    },
    g6c: function g6c () {
      // NOTE: fails
      return f('eight', 'four')
    }
  }
}

/* eslint-disable */
/* istanbul ignore next */
function g6d (x, y) {
  /* NOTE: What cases for x and y would we need to verify
           that g6d calls f6 correctly?
           And how would we check that? */
  const a = someMagic(x)
  const b = wizardry(a, x, y)
  return f6(a, b)
}
/* eslint-enable */

module.exports = g6
