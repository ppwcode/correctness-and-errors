/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

const assert = require('assert')

/**
 * [Kobayashi Maru](https://en.wikipedia.org/wiki/Kobayashi_Maru)
 *
 * @param {number} a
 * @return {string}
 */
function k4 (a) {
  assert(a === 1 || a === 2)

  if (a === 1) {
    return 'alpha'
  }
  assert(a === 2)
  return 'beta'
}

module.exports = k4
