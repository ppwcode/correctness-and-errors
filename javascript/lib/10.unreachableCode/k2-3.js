/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

const assert = require('assert')

/**
 * @param {number} a
 * @return {string}
 */
function k2 (a) {
  assert(a === 1 || a === 2)

  if (a === 1) {
    return 'alpha'
  } else if (a === 2) {
    return 'beta'
  } else {
    // NOTE: unreachable
    return 'should not happen'
  }
}

/**
 * @param {number} a
 * @return {string}
 */
function k3 (a) {
  assert(a === 1 || a === 2)

  if (a === 1) {
    return 'alpha'
  } else if (a === 2) {
    return 'beta'
  }
  // NOTE: unreachable
  return 'should not happen'
}

module.exports = { k2, k3 }
