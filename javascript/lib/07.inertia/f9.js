/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* global results:true */

const assert = require('assert')

/**
 * @param {number} a
 * @param {number} b
 * @return {number} a positive rational number (ℚ0+)
 * @return - results[results.length - 1] === result
 */
function f9 (a, b) {
  assert(Number.isInteger(a))
  assert(a >= 0)
  assert(Number.isInteger(b))
  assert(b >= 0)
  // assert(Array.isArray(results)) - possible, but not necessary

  const result = a / b
  assert(typeof result === 'number') // in computers, all numbers are rationals
  assert(result >= 0, 'result must be a _positive_ rational')
  global.results = [] // I want to make sure I can reach my postconditions
  results.push(result)
  return result
}

/* eslint-disable */
/* istanbul ignore next */
function g9 () {
  const q1 = f9(8, 4)
  const q2 = f9(8, 2)
  assert(results[results.length - 2] === q1) // NOTE: User expects this to be true
  assert(results[results.length - 1] === q2)
}
/* eslint-enable */

module.exports = f9
