/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* global quotients:true */

const assert = require('assert')

/**
 * @param numerator - a natural number (ℕ)
 * @param denominator - a natural number (ℕ)
 * @return - a positive rational number (ℚ0+)
 * @return - quotients[quotients.length - 1] === result
 */
function f1 (numerator, denominator) {
  const result = numerator + denominator // NOTE: + is ok
  quotients.push(result)
  return result
}

/* eslint-disable */
/* istanbul ignore next */
function g1 () {
  {
    const n = 8
    const d = 4
    const q = f(n, d)
    assert(quotients[quotients.length - 1] === q)
  }
}
/* eslint-enable */

module.exports = f1
