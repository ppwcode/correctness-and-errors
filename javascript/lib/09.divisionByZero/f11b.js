/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* global results:true */

const assert = require('assert')
const numberDiv = require('../_util/numberDiv')

/**
 * @param {number} a
 * @param {number} b
 * @return {number} a positive rational number (ℚ0+)
 * @return - results[results.length - 1] === result
 * @return - results === old(results)
 * @return - results.length === old(results.length + 1)
 * @return - ∀ i, i is natural, i < old(results.length): quotients[i] === old(results[i])
 */
function f11b (a, b) {
  assert(Number.isInteger(a))
  assert(a >= 0)
  assert(Number.isInteger(b))
  assert(b > 0) // NOTE: avoid DivisionByZero
  assert(Array.isArray(results))

  const result = numberDiv(a, b) // NOTE: throws DivisionByZero
  assert(typeof result === 'number') // in computers, all numbers are rationals
  assert(result >= 0, 'result must be a _positive_ rational')
  results.push(result)
  return result
}

/* eslint-disable */
/* istanbul ignore next */
function g11 () {
  let q = f11b(8, 0) // NOTE: forbidden
}
/* eslint-enable */

module.exports = f11b
