/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* global results:true */

/**
 * @callback push
 * @param element
 * @return - this.length === old(this.length) + 1
 * @return - this[this.length - 1] === element
 */

/**
 * @typedef Array
 * @property length - a natural number (ℕ)
 * @property {push} push
 */

/**
 * ### Extra preconditions // NOTE: spec what is needed
 *
 * - `results` is an Array
 *
 * @param {number} a - a natural number (ℕ)
 * @param {number} b - a natural number (ℕ)
 * @return {number} a positive rational number (ℚ0+)
 * @return - results[results.length - 1] === result
 */
function f5Types (a, b) {
  const result = a / b
  results.push(result) // NOTE: now this can be realized
  return result
}

module.exports = f5Types
