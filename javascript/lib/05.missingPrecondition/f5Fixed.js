/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* global results:true */

/**
 * ### Extra preconditions // NOTE: spec what is needed
 *
 * - `results` exists
 * - `results.length` is a natural number (ℕ)
 * - `results` has a `push(element)` method, that adds `element`, so that afterwards
 *   - results.length === old(results.length) + 1
 *   - results[results.length - 1] === element
 *
 * @param a - a natural number (ℕ)
 * @param b - a natural number (ℕ)
 * @return - a positive rational number (ℚ0+)
 * @return - results[results.length - 1] === result
 */
function f5Fixed (a, b) {
  const result = a / b
  results.push(result) // NOTE: now this can be realized
  return result
}

module.exports = f5Fixed
