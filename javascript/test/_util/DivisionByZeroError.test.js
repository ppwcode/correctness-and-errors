/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const DivisionByZeroError = require('../../lib/_util/DivisionByZeroError')

describe('_util/DivisionByZeroError', function () {
  it('constructs an error with a number', function () {
    const numerator = 8
    const result = new DivisionByZeroError(numerator)
    result.should.be.an.instanceOf(DivisionByZeroError)
    result.should.be.an.Error()
    result.numerator.should.equal(numerator)
    result.message.should.be.empty()
  })
})
