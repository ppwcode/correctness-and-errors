/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const numberDiv = require('../../lib/_util/numberDiv')

describe('_util/numberDiv', function () {
  it('divides with numbers', function () {
    const numerator = -4
    const denominator = Math.E
    const expected = numerator / denominator
    const result = numberDiv(numerator, denominator)
    result.should.be.a.Number()
    result.should.not.be.NaN()
    result.should.not.be.Infinity()
    result.should.equal(expected)
  })
  it('throws with denominator 0, and a good numerator', function () {
    const numerator = -4
    const denominator = 0
    numberDiv.bind(null, numerator, denominator).should.throw({ numerator })
  })
  it('throws with denominator and numerator 0', function () {
    const numerator = 0
    const denominator = 0
    numberDiv.bind(null, numerator, denominator).should.throw({ numerator })
  })
})
