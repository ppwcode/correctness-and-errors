/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */
/* global results:true */

const f = require('../../lib/09.divisionByZero/f11a')
const generateFTests = require('../_generateFTests-noConfusion')
const testName = require('../_testName')
const { goodWith0Numbers } = require('../_cases')

generateFTests(testName(module), f)

describe(`${testName(module)} — b = 0`, function () {
  beforeEach(function () {
    // noinspection JSUndefinedPropertyAssignment
    /**
     * @type {Array<number>}
     */
    global.results = [5, 8, -5, Math.E, 0] // eslint-disable-line no-global-assign
  })

  afterEach(function () {
    // noinspection JSUnresolvedVariable
    delete global.results
  })

  goodWith0Numbers.forEach(a => {
    it(`throws for f(${a}, 0)`, function () {
      const oldQuotients = results.slice()
      const result = f(a, 0)

      console.log(`f(${a}, ${0}) = ${result}`)

      result.should.be.a.Number()
      result.should.be.greaterThanOrEqual(0)

      results.should.be.an.Array()
      results.length.should.equal(oldQuotients.length + 1)
      oldQuotients.push(result)
      results.should.deepEqual(oldQuotients)
    })
  })
})
