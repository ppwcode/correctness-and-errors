/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */

const f = require('../../lib/06.verifyPreconditions/fAssertBad')
const generateFTests = require('../_generateFTests-noConfusion')
const testName = require('../_testName')

generateFTests(testName(module), f)

describe('fAssertBad2', function () {
  afterEach(function () {
    delete global.results
  })

  it('covers the first pre', function () {
    f.bind(-3, 6).should.throw()
  })

  it('covers the second pre', function () {
    f.bind(6, -3).should.throw()
  })

  it('covers the third pre', function () {
    // MUDO for some reason, this does not cover
    global.results = 'I am not an array'
    f.bind(3, 6).should.throw()
  })
})
