/*
  Copyright 2019 - 2021 PeopleWare n.v.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/* eslint-env mocha */
/* global quotients:true */

const { goodCases } = require('./_cases')

/**
 * @callback casesFilter
 * @param {object} case
 * @param {number} case.numerator
 * @param {number} case.denominator
 * @returns {boolean}
 */

/**
 * @param {string} name
 * @param {f} f
 */
function generateFTests (name, f) {
  describe(`${name}`, function () {
    beforeEach(function () {
      // noinspection JSUndefinedPropertyAssignment
      /**
       * @type {Array<number>}
       */
      global.quotients = [5, 8, -5, Math.E, 0] // eslint-disable-line no-global-assign
    })

    afterEach(function () {
      // noinspection JSUnresolvedVariable
      delete global.quotients
    })

    goodCases.forEach(c => {
      it(`works for f(${c.a}, ${c.b})`, function () {
        const oldQuotients = quotients.slice()
        const result = f(c.a, c.b)

        console.log(`f(${c.a}, ${c.b}) = ${result}`)

        result.should.be.a.Number()
        result.should.be.greaterThanOrEqual(0)

        quotients.should.be.an.Array()
        quotients.length.should.equal(oldQuotients.length + 1)
        oldQuotients.push(result)
        quotients.should.deepEqual(oldQuotients)
      })
    })
  })
}

module.exports = generateFTests
