const x = require('cartesian')

/**
 * @type {Array<Number>}
 */
const goodNumbers = [1, 4, 12, Number.MAX_SAFE_INTEGER]

/**
 * @type {Array<Number>}
 */
const goodWith0Numbers = [0].concat(goodNumbers)

/**
 * @typedef {object} Case
 * @property {number} a
 * @property {number} b
 */

/**
 * @type {Array<Case>}
 */
const goodCases = x({
  a: goodWith0Numbers,
  b: goodNumbers
})

/**
 * @type {Array<Case>}
 */
const bSmallerThenA = goodCases.filter(c => c.b <= c.a)

/**
 * @type {Array<Case>}
 */
module.exports = { goodWith0Numbers, goodCases, bSmallerThenA }
