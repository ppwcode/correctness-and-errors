﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace correctness_and_errors
{
  public class Main
  {
    public static readonly List<double> quotients = new List<double>();
    
    public static double f(int numerator, int denominator)
    {
      Debug.Assert(numerator >= 0);
      Debug.Assert(denominator >= 0);
      Debug.Assert(quotients != null);
      
      double result = numerator / denominator;
      Debug.Assert(result >= 0);
      quotients.Add(result);
      return result;
    }
  }
}
