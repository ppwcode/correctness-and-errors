﻿using System;
using System.Collections.Generic;
using correctness_and_errors;
using NUnit.Framework;

namespace test
{
  public class MainTest
  {
    public static int[] intCases = new[] {0, 1, 4, 12, Int32.MaxValue};
    
    public static IEnumerable<TestCaseData> TestCases
    {
      get
      {
        foreach (var numerator in intCases)
        {
          foreach (var denominator in intCases)
          {
            yield return new TestCaseData(numerator, denominator);
          }
        }
      }
    }
    
    [TestCaseSource(nameof(TestCases))]
    public void testF(int numerator, int denominator)
    {
      List<double> oldQuotients = new List<double>(Main.quotients);
      try
      {
        double result = Main.f(numerator, denominator);
        Assert.That(result, Is.GreaterThanOrEqualTo(0));
        Assert.That(Main.quotients, Is.Not.Null);
        Assert.That(Main.quotients.Count, Is.EqualTo(oldQuotients.Count + 1));
        for (int i = 0; i < oldQuotients.Count; i++)
        {
          Assert.That(oldQuotients[i], Is.EqualTo(Main.quotients[i]));
        }
        Assert.That(result, Is.EqualTo(Main.quotients[Main.quotients.Count - 1]));
      }
      catch (DivideByZeroException e)
      {
        Assert.That(denominator, Is.EqualTo(0));
        Assert.That(Main.quotients, Is.Not.Null);
        Assert.That(Main.quotients.Count, Is.EqualTo(oldQuotients.Count));
        for (int i = 0; i < oldQuotients.Count; i++)
        {
          Assert.That(oldQuotients[i], Is.EqualTo(Main.quotients[i]));
        }
      }
    }
  }
}
